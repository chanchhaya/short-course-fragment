package com.chhaya.fragment2_4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import com.chhaya.fragment2_4.fragments.FragmentOne;
import com.chhaya.fragment2_4.fragments.MainSettingFragment;
import com.chhaya.fragment2_4.helper.FragmentHelper;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        if (Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
            FragmentHelper.add(this, R.id.f_container, new MainSettingFragment());
        } else if (Configuration.ORIENTATION_LANDSCAPE == getResources().getConfiguration().orientation) {
            FragmentHelper.add(this, R.id.fl_container, new MainSettingFragment());
            FragmentHelper.add(this, R.id.fr_container, new FragmentOne());
        }



    }
}
