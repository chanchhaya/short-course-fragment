package com.chhaya.fragment2_4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;

import com.chhaya.fragment2_4.fragments.DetailFragment;
import com.chhaya.fragment2_4.fragments.HomeFragment;
import com.chhaya.fragment2_4.fragments.MainSettingFragment;
import com.chhaya.fragment2_4.helper.FragmentHelper;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class TabActivity extends AppCompatActivity {

    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        tabLayout = findViewById(R.id.app_tab);

        final HomeFragment homeFragment = new HomeFragment();
        FragmentHelper.add(this, R.id.tab_container, homeFragment);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                Fragment fragment = null;
                switch (position) {
                    case 0:
                        fragment = new HomeFragment();
                        break;
                    case 1:
                        fragment = new DetailFragment();
                        break;
                    case 2:
                        fragment = new MainSettingFragment();
                        break;
                }

                if (fragment != null) {
                    FragmentHelper.replace(TabActivity.this,
                            R.id.tab_container, fragment);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}
