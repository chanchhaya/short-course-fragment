package com.chhaya.fragment2_4.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.chhaya.fragment2_4.fragments.DetailFragment;
import com.chhaya.fragment2_4.fragments.HomeFragment;
import com.chhaya.fragment2_4.fragments.MainSettingFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private int tabCount;

    public ViewPagerAdapter(@NonNull FragmentManager fm, int tabCount) {
        super(fm, tabCount);
        this.tabCount = tabCount;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new HomeFragment();break;
            case 1:
                fragment = new DetailFragment(); break;
            case 2:
                fragment = new MainSettingFragment();break;
        }
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence title = "";
        switch (position) {
            case 0:
                title = "Home";break;
            case 1:
                title = "About";break;
            case 2:
                title = "Setting";break;
        }
        return title;
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
