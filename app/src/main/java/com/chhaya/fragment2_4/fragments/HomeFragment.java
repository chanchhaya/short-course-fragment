package com.chhaya.fragment2_4.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.chhaya.fragment2_4.R;

public class HomeFragment extends Fragment {

    private HomeFragmentListener listener;
    private EditText editMsg;
    private Button btnSend;

    public interface HomeFragmentListener {
        void sendMsg(String msg);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (HomeFragmentListener) context;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }

    // initialize view
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewFragment = inflater.inflate(R.layout.home_fragment_layout, null);
        editMsg = viewFragment.findViewById(R.id.edit_homebox);
        btnSend = viewFragment.findViewById(R.id.button_homebox);
        return viewFragment;
    }

    // bind data
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = editMsg.getText().toString();
                listener.sendMsg(msg);
                hideKeyboardFrom(getContext(), editMsg);
            }
        });
    }

    private static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
