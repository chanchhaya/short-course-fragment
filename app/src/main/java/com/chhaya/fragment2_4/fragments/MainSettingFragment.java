package com.chhaya.fragment2_4.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.chhaya.fragment2_4.R;
import com.chhaya.fragment2_4.SettingActivity;
import com.chhaya.fragment2_4.helper.FragmentHelper;

public class MainSettingFragment extends Fragment {

    private ListView lvMainSetting;
    private String[] settingName = {"General", "Network", "Application", "Wallpaper", "About"};

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_setting_fragment_layout, container, false);
        lvMainSetting = view.findViewById(R.id.main_setting_list);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,
                settingName);
        lvMainSetting.setAdapter(adapter);

        lvMainSetting.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("TAG", parent.getItemAtPosition(position) + "");
                if (position == 0) {
                    if (getActivity() instanceof SettingActivity) {
                        if (Configuration.ORIENTATION_PORTRAIT == getResources().getConfiguration().orientation) {
                            FragmentHelper.replace((SettingActivity) getActivity(), R.id.f_container, new FragmentOne());
                        }
                    }
                }
            }
        });
    }
}
